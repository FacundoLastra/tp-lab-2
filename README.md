Tp lab 2


Ciclo de ejecución de los metodos del patrón usado

1ro: se instancia el observable
2do: se agrega a los observadores en el observable
3ro: se realiza un cambio dentro del observable
4to: dentro del método del observable para realizar un cambio en si mismo, luego de realizar el cambio por ejemplo modificar el atributo nombre, se realiza un setChanged(); para declarar que hubo un cambio en el observable
5to:se realiza un notifyObservers(); este método se puede elegir llamarla con parametros, enviando lo que a cambiado o no, avisando a todos los observadores que se a realizado un cambio.
6to:se ejecutan los metodos Update(); en los observadores.

Que argumentos se pasan al método Update() y en que momento se ejecuta dicho método.

el update siempre recibe el objeto observado como parametro y se puede elegir enviarle un parametro adicional, mediante sobrecarga de metodos.
este parametro adicional puede ser por ejemplo el valor que a cambiado dentro del objeto observado.

se ejecuta despues de llamar al metodo notifyObservers(); y es obligatorio que antes se de llamar a este método se declare que hubo cambios con el método setChanged(); (para que se ejecute el metodo update();).