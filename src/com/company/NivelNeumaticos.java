package com.company;

/**
 * Created by SkyDoo on 26/3/2018.
 */
public class NivelNeumaticos extends Sensor {
    public NivelNeumaticos(double actual) {
        super(actual);
    }
    public String toString() {
        return "El Nivel de Aire en los Neumaticos a cambiado, Valor nuevo: "+this.actual + " El valor anterior era: "+this.anterior;
    }
}
