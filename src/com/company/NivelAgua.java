package com.company;

/**
 * Created by SkyDoo on 26/3/2018.
 */
public class NivelAgua extends Sensor {
    public NivelAgua(double actual) {
        super(actual);
    }
    public String toString() {
        return "El Nivel de Agua a cambiado, Valor nuevo: "+this.actual + " El valor anterior era: "+this.anterior;
    }
}
