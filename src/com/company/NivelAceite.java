package com.company;

/**
 * Created by SkyDoo on 26/3/2018.
 */
public class NivelAceite extends Sensor{


    public NivelAceite(double actual) {
        super(actual);
    }

    @Override
    public String toString() {
        return "El Nivel de Aceite a cambiado, Valor nuevo: "+this.actual + " El valor anterior era: "+this.anterior;
    }
}
