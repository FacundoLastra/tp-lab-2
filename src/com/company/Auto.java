package com.company;

import java.util.Observable;

/**
 * Created by SkyDoo on 26/3/2018.
 */
public class Auto extends Observable {

    String patente;
    Sensor nAceite;
    Sensor pNeumaticos;
    Sensor nAgua;

    public Auto(String patente, double nAceite, double pNeumaticos, double nAgua) {
        this.patente = patente;
        this.nAceite = new NivelAceite(nAceite);
        this.pNeumaticos = new NivelNeumaticos(pNeumaticos);
        this.nAgua = new NivelAgua(nAgua);
    }

    public String getPatente() {
        return patente;
    }

    public void setnAceite(double nAceite) {

        this.nAceite.actualizarSensor(nAceite);
        setChanged();
        notifyObservers(this.nAceite);
    }



    public void setpNeumaticos(double pNeumaticos) {
        this.pNeumaticos.actualizarSensor(pNeumaticos);
        setChanged();
        notifyObservers(this.pNeumaticos);
    }



    public void setnAgua(double nAgua) {
        this.nAgua.actualizarSensor(nAgua);
        setChanged();
        notifyObservers(this.nAgua);
    }
}
