package com.company;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by SkyDoo on 26/3/2018.
 */
public class Mecanico implements Observer {
    public Mecanico() {
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Auto){
            Auto a = (Auto)o;
            System.out.println("En el auto: "+a.patente);
        }
        System.out.println(arg.toString());
    }
}
