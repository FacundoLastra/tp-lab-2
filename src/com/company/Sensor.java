package com.company;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by SkyDoo on 26/3/2018.
 */
public abstract class Sensor {

    double actual;
    double anterior;

    public Sensor(double actual) {
        this.actual = actual;
        this.anterior=-1; ///valor irracional
    }

    public void actualizarSensor(double valor){
        if (valor >= 0){
            this.anterior=this.actual;
            this.actual=valor;
        }
    }
    public abstract String toString();


}
